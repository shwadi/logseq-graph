#!/usr/bin/env bash

cd "$(dirname "$0")"/..
find pages -name '*.md' | xargs -I% bash -c 'tail -1 "%" | grep -q "^\s*-\s*$" && echo %'
