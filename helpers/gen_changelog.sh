#!/usr/bin/env bash

GITLAB_REPO_LINK='https://gitlab.com/shwadi/logseq-graph'
urlunqoute(){
    local pythoncode=$(cat<<EOF
import sys
from urllib.parse import unquote

print(unquote(sys.stdin.read()),end='')
EOF
)
    python3 -c "$pythoncode"
}

gen_filelink(){
    local file=$1
    local filelink="$GITLAB_REPO_LINK/-/blob/$commithash/$file"
    echo "[$file]($filelink)"
}

gen_pageref_if_file_is_page(){
    local file=$1
    page_reference="" #reset
    if grep -q '^pages/.*\.md' <<< "$file"; then
        if [ -f "$file" ]; then
            page_reference=$( echo $file | sed -e 's#^pages/##' -e 's#___#/#g' -e 's#\.md$##' | urlunqoute )
            page_reference="[[$page_reference]]"
        else
            page_reference=' ⚠ `page does not exist anymore`'
        fi
    fi
    [ -n "$page_reference" ] # generates return value
}

main(){
    local last_timestamp timestamp date commithash current_date
    while read timestamp _ date commithash; do
        if [ -z "$last_timestamp" ] || [ "$last_timestamp" -gt "$timestamp" ]; then
            last_timestamp=$timestamp
            if [ "$current_date" != "$date" ]; then
                # start a new date entry
                current_date=$date
                echo "- # $date"
            fi
        else
            : # do nothing
            # if the previous commit is older than the current, simply append the commit
            # message to the day the previous commit was created
        fi
        git show -s $commithash | sed 1,4d | sed '1s/   /  -/'
        echo "    - [diff]($GITLAB_REPO_LINK/-/commit/$commithash)"
        echo "    - touched files:"

        local file status f1 f2
        while IFS=$'\t' read status f1 f2; do
            local flag=$(sed -E 's/^(.).*/\1/g' <<< "$status")
            local action
            case "$flag" in
                A) action=added; ;;
                M) action=modified; ;;
                R) action=renamed; ;;
                D) action=deleted; ;;
                *) action=""; ;;
            esac
            local filerelated_infotext="$action $(gen_filelink "$f1")"
            local page_reference
            if gen_pageref_if_file_is_page "$f1"; then
                filerelated_infotext+=" $page_reference"
            fi
            if [ -n "$f2" ]; then
                filerelated_infotext+=' -> '
                filerelated_infotext+="$(gen_filelink "$f2")"
                if gen_pageref_if_file_is_page "$f2"; then
                    filerelated_infotext+=" $page_reference"
                fi
            fi
            echo "      - $filerelated_infotext"
        done <<< $(git show --name-status --format='' $commithash)
    done <<< "$( git log --date=raw --pretty=format:'%cd %cs %H' )"
}

main > "$(dirname "$0")/../pages/changelog.md"
