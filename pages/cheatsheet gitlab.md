- cicd
  - fetch the entire repository within the pipeline
    - the latter of the two solutions overrules the prior:
      - set `Git Shallow Clone` to `0` under the `Settings`->`CICD`->`General Pipeline`
      - set the `GIT_DEPTH` job variable to `0`
  - Download the artifacts zipped archive from the latest **successful** pipeline for the given reference name and job
    - [source](https://docs.gitlab.com/ee/api/job_artifacts.html#download-the-artifacts-archive)
    - ```bash
      curl --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "https://gitlab.example.com/api/v4/projects/$PROJECT_ID/jobs/artifacts/$REF_NAME/download?job=$JOB_NAME" -o dest.zip
      ```
      - `REF_NAME`: Branch or tag name in repository. HEAD or SHA references are not supported.
      - header is not required for public projects
        - for private projects the generated `ACCESS_TOKEN` requires solely `Guest` role and `read_api` scope
      - `PROJECT_ID`: predefined within pipeline as `CI_PROJECT_ID`
      - `JOB_NAME` is the full job name; not to confuse with stage