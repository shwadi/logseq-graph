- Quite interesting book I heard of in [Episode 64 of the Jordan Harbinger show](http://web.archive.org/web/20220701204701/https://www.jordanharbinger.com/sean-young-changing-your-life-for-good-with-science/)
  - the notes here are based on this source
- Two-Step Process for Change
  - steps
    - step 1: identify the type of behavior you aim to change: A, B, or C.
      - A: automatic behavior
        - explanation:
          - Things you do not have conscious awareness over
        - example:
          - biting nails
      - B: burning behavior:
        - explanation:
          - burning and nearly irresistible desire to do something in a more or less conscious manner
          - can cause obsession
        - example:
          - checking mails
          - checking your [[spyphone]]
      - C: common behavior:
        - explanation:
          - things you do repeatedly and consciously
          - most common behaviors people try to change
          - less deep-seated as the above behavior types
          - do not cause obsession
        - example:
          - lack of motivation for things you know you would benefit from like:
            - go to bed early to get enough sleep
            - eat healthy
            - exercising more frequently
    - step 2: identify the forces or tools for changing the behavior using the [[SCIENCE model of lasting change]]
  - Integration between behavior types and [[SCIENCE model of lasting change]]
    - |            | Automatic  | Burning |  Common|
      |------------|------------|---------|--------|
      |Stepladders |            | T       |  S     |
      |Community   |            | T       |  P     |
      |Important   |            | T       |  S     |
      |Easy        | P          | P       |  S     |
      |Neurohacks  | S          | S       |  T     |
      |Captivating | S          | S       |  S     |
      |Engrained   | P          | P       |  S     |
      - Legend:
        - P = Primary
        - S = Secondary
        - T = Tertiary
      - explanation
        - > Primary methods will be the most important to changing the behavior while secondary will be second and tertiary will be third.
          > For example, for A behaviors you will use the Easy and Engrained tools first, then secondary will be Neurohacks and Captivating.