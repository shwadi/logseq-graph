- Ways to prune images in the internal registry
  - `oc delete image <sha256:image-id>` only removes the image from etcd, but not from the registry’s storage
    - to fully prune such images, [hard pruning](https://docs.openshift.com/container-platform/4.12/applications/pruning-objects.html#pruning-hard-pruning-registry_pruning-objects) is required
  - `oc adm prune images` can prune images from internal registry but it [does not take any specific tag version as an input, so as a workaround use the "time" as a factor to identify the old tagged images](https://access.redhat.com/solutions/6977682)
    - example
      - ```bash
        oc adm prune images --keep-tag-revisions=2 --keep-younger-than=2400h
        ```
        - with `keep-tag-revisions`, the most recent items in `stream.status.tags[].items` are meant (1)
        - `keep-tag-revisions=0` would delete not only old revisions of all tags, but the tags as well (1)
        - (1): [docs.openshift](https://docs.openshift.com/container-platform/3.11/admin\_guide/pruning\_resources.html)