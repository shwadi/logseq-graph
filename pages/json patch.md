- notes on `target location` specification:
  - must start with `/`
  - indexes do not require `[]`
  - the following chars need to be escaped:
    - `~` -> `~0`
    - `/` -> `~1`
- Operations
  - `add`
    - required params:
      - `path`: defines the `target location`
      - `value`
    - if `target location` specifies
      - array **index** -> append `value`
        - `-` : indexes the end of an array
      - an object member that ...
        - does not already exist -> add new member
        - does     already exist -> replace the value
  - `remove`
    - required params:
      - `path`
  - `replace`
    - required params:
      - `path` the target location **must** exist
      - `value`
  - `move`
    - required params:
      - `path`
      - `from` a location cannot be moved into one of its children
    - operation is functionally identical to (`remove` + `add`) operation
  - `copy`
    - required params:
      - `path`
      - `from`
    - operation is functionally identical to an `add` operation
  - `test`
    - required params:
      - `path`
      - `value`
- example:
  - usage in `oc`/`kubectl`
    - ```bash
      oc patch cm ca-cert --type=json --patch='
          [
            {
              "op": "move",
              "from": "/data/ca-bundle.crt",
              "path": "/data/ca-certificates.crt"
            },
            {
              "op": "remove",
              "path": "/metadata/labels/config.openshift.io~1inject-trusted-cabundle"
            }
          ]
      '
      ```
  - usage in [[kustomize]]
    - example `kustomization.yaml` content:
      - ```yaml
        kustomization.yaml
        apiVersion: kustomize.config.k8s.io/v1beta1
        kind: Kustomization
        resources:
          - ...
        patches:
          - target:
              kind: Test
            patch: |-
              - op: replace
                path: /spec/something
                value: 123
              - op: replace
                path: /spec/something-else
                value: 123
        
          - target:
              kind: Anotertest
            path: patchfile.yaml
        ```
    - example `patchfile.yaml` content:
      - ```yaml
        op: replace
        path: /spec/something
        value: 123
        ```
- resources
  - https://datatracker.ietf.org/doc/html/rfc6902