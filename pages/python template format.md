- structure:
  - ```
    '{<attribute_name | element_index>:<format_spec>}'.format()
    ```
    - See [format-string-syntax](https://python.readthedocs.io/en/stable/library/string.html#format-string-syntax) for more details
    - `format_spec` ([complete specification](https://python.readthedocs.io/en/stable/library/string.html#format-specification-mini-language))
      - fill (default: space)
      - alignment (`<` left, `>` right, `^` center)
      - signhandling (`+`: put sign on all nums; `-`: put sign on neg. naums, ` `: space represent pos. nums)
      - suppress `0x`, `0o`, `0b` Prefix by adding `#`
      - add leading zeroes by adding `0`
      - width specifications for integers and floating-point nums
      - .precision specification for floating-point nums
      - type
        - string
          - `s`
        - integer
          - `x`: base 16 (lowercase)
          - `X`: base 16 (uppercase)
          - `d`: base 10
          - `o`: base 8
          - `b`: base 2
          - `c`: translate to unicode
        - floating point
          - `%`: multiply by 100 and add '%'
          - see [format_spec](https://python.readthedocs.io/en/stable/library/string.html#format-specification-mini-language) 4 more
          -
  - useful in combination with `locals()` / `vars()`
    - ```python
      '...'.format(**locals())
      '...'.format_map(locals())
      ```