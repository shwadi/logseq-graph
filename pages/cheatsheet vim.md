- spellchek
  - activate: `:set spell`
  - deactivate: `:set nospell`
  - define language: `:set spelllang=en_us`
  - move to next/previous misspelled word: `]s` / `[s`
  - show spell suggestions: `z=`
  - mark word as good/wrong spelled: `zg` / `zw`
- noninteractive mode
  id:: 65e8c367-b1eb-44a2-8e89-d7f0bd6f5f0e
  - commands can be passed via
    - option `-es` followed by an arbitrary amount of commands passed one of the following ways
      - `+<command>`
      - -c `<command>`
    - arg `-s </path/to/file_containing_commands>`
- using vim as filter in a pipe (useful in combination with ((65e8c367-b1eb-44a2-8e89-d7f0bd6f5f0e)))
  - ```bash
    seq 3 | vim -es '+:1d' '+:wq! /dev/stdout' /dev/stdin
    ```
  - ```bash
    seq 3 | vim -es -c ':1d' -c '%print' -c ':q!' /dev/stdin
    ```
- Command to diff the current buffer against original file (i.e. changes since opening or within an opened `:recover!` fileversion)
  - `:DiffOrig`
    - if not already defined in `$VIMRUNTIME/defaults.vim`:
    - ```vim
        command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
      		  \ | wincmd p | diffthis
      ```
- tasks
  - activate visual highlighting of the screen column (usefull for yaml editing) #card
    card-last-interval:: 33.64
    card-repeats:: 4
    card-ease-factor:: 2.9
    card-next-schedule:: 2024-07-09T09:07:20.178Z
    card-last-reviewed:: 2024-06-05T18:07:20.181Z
    card-last-score:: 5
    - cursorcolumn
      - `:set cuc`
      - if highlight does not become visible, try launching vim with env definition `TERM="screen-256color"` or `TERM="tmux-256color`
        - if this works, persist the configuration (either)
          - as export term in `~/.bashrc`
          - as config entry in `~/.tmux.conf`
            - ```
              set-option -g default-terminal tmux-256color
              ```
        - make sure to choose the right `TERM` specification
          - persisting `TERM="screen-256color` in `tmux` will cause issues
            - example: cursive noncolor highlighting of searchterms in less
    - static column(s)
      - `:set colorcolumn=3` ... `,5,7,...`
  - determine buffer number of current window #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:05:31.064Z
    card-last-reviewed:: 2024-06-05T18:05:31.066Z
    card-last-score:: 5
    - `:echo bufnr('%')`
    - `2` + `ctrl g`
  - open another buffer in current window #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:03:16.962Z
    card-last-reviewed:: 2024-06-05T18:03:16.963Z
    card-last-score:: 5
    - `:b <bufnum>` use `<tab>` if bufnum is unknown
  - close all **windows** except the current one #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:07:32.997Z
    card-last-reviewed:: 2024-06-05T18:07:32.998Z
    card-last-score:: 5
    - `:on[ly]`
  - close all **tabs** except the current one #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:05:37.043Z
    card-last-reviewed:: 2024-06-05T18:05:37.045Z
    card-last-score:: 5
    - `:tabon[ly]`
  - move **tab** to `#th` position #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:17:26.853Z
    card-last-reviewed:: 2024-06-05T18:17:26.855Z
    card-last-score:: 5
    - `:tabmove #`
  - `:help ins-completion` shortcuts to complete **line** and **dictionary** #card
    card-last-score:: 5
    card-repeats:: 5
    card-next-schedule:: 2024-08-13T22:13:52.783Z
    card-last-interval:: 84.1
    card-ease-factor:: 2.76
    card-last-reviewed:: 2024-05-21T20:13:52.783Z
    - whole lines: `i_CTRL-X_CTRL-L`
    - keyworts in dictionary: `i_CTRL-X_CTRL-K`
  - shortcut to abort completion mode `:help ins-completion`
    - `ctrl e`
  - get value of a setting #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:12:32.634Z
    card-last-reviewed:: 2024-06-05T18:12:32.635Z
    card-last-score:: 5
    - hit `:set <setting>?`
    - use `<tab>` as in: `:set iskeyword=<tab>`
    - or via ((65f76985-7e13-4ca7-95b9-5c63b8afa45b))
  - save current settings into a file
    - `help save-settings`
    - `:mk <file>`
  - in command mode:
    - You typed the first letters of a command you do not fully remember.
      card-last-interval:: 108
      card-repeats:: 5
      card-ease-factor:: 3
      card-next-schedule:: 2024-09-21T18:03:50.778Z
      card-last-reviewed:: 2024-06-05T18:03:50.779Z
      card-last-score:: 5
      How to print all commands starting with the entered pattern? #card
      - `:help c_CTRL-A`
      - better: hit `tab` and navigate through the list via `c_CTRL-N` and `c_CTRL-P`
    - List all options #card
      card-last-interval:: 84.1
      card-repeats:: 5
      card-ease-factor:: 2.76
      card-next-schedule:: 2024-08-13T22:14:18.724Z
      card-last-reviewed:: 2024-05-21T20:14:18.724Z
      card-last-score:: 5
      id:: 65f76985-7e13-4ca7-95b9-5c63b8afa45b
      - `:set all`
      - `:options` handy options browser
    - pull word unter the cursor into command line
      - `:help c_CTRL-R_CTRL-F` or `:help c_CTRL-R_CTRL-P` for filename extended with path
  - undo history
    - Go to older text state since the last write #card
      card-last-interval:: 33.64
      card-repeats:: 4
      card-ease-factor:: 2.9
      card-next-schedule:: 2024-07-09T09:02:30.475Z
      card-last-reviewed:: 2024-06-05T18:02:30.478Z
      card-last-score:: 5
      - `earliier 1f` oposit: `later 1f`
    - Keystroke to travel the `:undolist` #card
      card-last-interval:: 33.64
      card-repeats:: 4
      card-ease-factor:: 2.9
      card-next-schedule:: 2024-07-09T09:02:43.643Z
      card-last-reviewed:: 2024-06-05T18:02:43.643Z
      card-last-score:: 5
      - `g-` and `g+`