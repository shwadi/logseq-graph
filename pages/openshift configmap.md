- Max size is [limited by etcd](https://github.com/kubernetes/kubernetes/issues/19781) to 1 MiB #configmap #sizelimit
  - Configbased creation can reach the limit even sooner:
    ```bash
    $ dd if=/dev/urandom of=test bs=1M count=1
    
    # this does not work
    $ oc create cm test  --from-file test --dry-run=client -o yaml | oc apply -f -
    The ConfigMap "test" is invalid: metadata.annotations: Too long: must have at most 262144 bytes
    
    # this works
    $ oc create cm test  --from-file test
    configmap/test created
    ```