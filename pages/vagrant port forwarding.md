- forward `vm port` to `host port`
  - via configurationfile
    - ```ruby
      config.vm.network "forwarded_port", guest: <PORT>, host: <PORT>
      ```
      - if virtualbox is used as provider, this configuration create an extra port forward entry, which is observable in the extended network settings of the vm
      - the options:
        - `auto_correct: "True"`: change host port automatically if it is already in use (instead of throwing an error)
        - `guest` / `host`: guest/host port
        - `guest_ip`: the destination ip
          - if kept empty, the port will supposedly "go to every IP interface" [source1](https://docs.w3cub.com/vagrant/networking/forwarded_ports.html) [source2](https://developer.hashicorp.com/vagrant/docs/networking/forwarded_ports)
            - this sounds like BS; how can a connection have multiple endpoints?
            - fiddling around with the vbox settings showed that
              - specifying `127.0.0.1` does not forward to localhost for whatever reason
              - specifying the actual IP address works
              - specifying a remote IP (which is accessible from within the vm) does not work eather
        - `host_ip`: IP to bound the port to
          - if kept empty, it will be supposedly "bound to every IP" [source1](https://docs.w3cub.com/vagrant/networking/forwarded_ports.html) [source2](https://developer.hashicorp.com/vagrant/docs/networking/forwarded_ports)
            - in fact it is bound to localhost `ss -tln | grep <port>`
  - via ssh port forward #[[ssh port forward]]
    - ```bash
      vagrant ssh -- -L ...
      ```
- forward `host port` to `vm port` (reverse port forward)
  - via configurationfile: not possible
  - via ssh port forward: as above
  - use `Host-Only Network` as [an alternative solution](https://stackoverflow.com/questions/16244601/vagrant-reverse-port-forwarding#16420720)