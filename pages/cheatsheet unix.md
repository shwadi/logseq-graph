- how to combine `x` lines in a row #card
  card-last-interval:: 33.64
  card-repeats:: 4
  card-ease-factor:: 2.9
  card-next-schedule:: 2024-06-22T09:42:41.728Z
  card-last-reviewed:: 2024-05-19T18:42:41.729Z
  card-last-score:: 5
  - `xargs -nx`
- list files and folders in current dir sorted by their total size #card
  card-last-interval:: 33.64
  card-repeats:: 4
  card-ease-factor:: 2.9
  card-next-schedule:: 2024-06-22T09:43:33.399Z
  card-last-reviewed:: 2024-05-19T18:43:33.399Z
  card-last-score:: 5
  - `du -cks * | sort -rn`
  - `du -chs * | sort -rh` # human readable numbers format