tags:: #git, #issue, #submodule

- initialization error message:
  - > fatal: transport 'file' not allowed
- solutions ([source](https://bbs.archlinux.org/viewtopic.php?id=280571))
  - use `git submodule--helper` instead of `git submodule`
  - run `git config --global protocol.file.allow always` once