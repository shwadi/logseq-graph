- ```bash
  command 3>&2 2>&1 1>&3
  ```
  - use filedescriptor 3 to remember the stderr output location
  - point stderr to the same location as stdout
  - point stdout to the same location as fd3, which is the initial stderr location