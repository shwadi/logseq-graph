- access metrics from prometheus itself
  - oc -n openshift-monitoring rsh po/prometheus-k8s-0 curl -E /etc/prometheus/secrets/metrics-client-certs/tls.crt --key /etc/prometheus/secrets/metrics-client-certs/tls.key https://prometheus-k8s.openshift-monitoring.svc:9092/metrics
- query Prometheus
  - ```bash
    oc project openshift-monitoring
    query=...
    curl -s -H "Authorization: Bearer $(oc get secret -o name | grep k8s-token | head -1 | xargs oc get -o jsonpath='{.data.token}' | base64 -d)" https://$(oc get route prometheus-k8s -o yaml | yq -r .spec.host)/api/v1/query --data-urlencode "query=$query"
    ```
- record query logs
  - activate logfile
    - configuration:
      - ```bash
        oc project openshift-monitoring
        newcontent=$(oc get cm cluster-monitoring-config -o yaml | yq -r '.data."config.yaml"' | yq -y '.prometheusK8s.queryLogFile="/prometheus/the_filename_i_chosen"')
        patch=$(jq -n '[{"op": "replace", "path": "/data/config.yaml", "value":$foo}]' --arg foo "$newcontent")
        oc patch cm/cluster-monitoring-config --type=json -p="$patch"
        ```
    - make sure config will apply by deleting the according statefulset
      - ```bash
          oc delete statefulset.apps/prometheus-k8s
          ```
  - deactivate logfile
    collapsed:: true
    - like above with this variation:
      collapsed:: true
      - ```bash
          newcontent=$(oc get cm cluster-monitoring-config -o yaml | yq -r '.data."config.yaml"' | yq -y 'del(.prometheusK8s.queryLogFile)')
          ```
  - delete logfiles on all instances
    - ```bash
      oc get po -o name | grep k8s | xargs -I§ oc rsh § rm the_filename_i_chosen
      ```
  - watch logfiles of all instances simultaneously:
    collapsed:: true
    - ```bash
      exporter(){ oc rsh $1 tail -f the_filename_i_chosen | tr '\n' '\0' | xargs -0I§ echo "$1: §"; }
      export -f exporter
      (oc get po -o name | grep k8s | xargs -I§ echo 'exporter §&'; echo wait) | bash
      ```