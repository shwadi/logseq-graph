tags:: #vagrant, #ansible, #provisioning

- configuration snippet:
  - ```ruby
    config.vm.provision "ansible" do |ansible|
      ansible.playbook = "playbook.yaml"
    end
    ```