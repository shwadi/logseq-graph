- this does not work:
  - ```bash
    'te\'st'
    ```
- the quote needs to be terminated in any case
  - ```bash
    'te'\''st'
    'te'"'"'st'
    ```