- > The Dunning–Kruger effect is a cognitive bias in which people with limited competence in a particular domain overestimate their abilities.
  [...]
  In popular culture, the Dunning–Kruger effect is often misunderstood as a claim about general overconfidence of people with low intelligence instead of specific overconfidence of people unskilled at a particular task.
  - source: [wikipedia](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect)
- opposite of the [[impostor syndrome]]