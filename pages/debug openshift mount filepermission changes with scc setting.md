tags:: #[[security context constraint]], #scc, #mount, #filepermission

- Binding the pod-related ServiceAccount to a more powerful scc like `anyuid` leads to a strange behaviour of unaccessabel volumes
- reason
  - when utilizing `openshift.io/scc: restricted-v2` beyond others the following is being set:
    - `pod.spec.securityContext.fsGroup` and `pod.spec.containers.securityContext.runAsUser` are being assigned the same value which allows the user to access mounted volumes
  - The above settings are not applied with `openshift.io/scc: anyuid` therefore:
    - the default uid of the image is being used
    - the mounted volumes are owned by root