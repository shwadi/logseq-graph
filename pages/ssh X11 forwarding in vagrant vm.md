tags:: #ssh, #X11, #forward, #vagrant, #vm, #cheatsheet

- Add the following lines to Vagrantfile:
  
  ```
  # config.vm.box = "generic/debian10" # just a hint to a potential vm
  config.ssh.forward_agent = true
  config.ssh.forward_x11 = true
  ```
- `vagrant ssh` into the vm and ...
  - make sure xauth is installed
    ```bash
    which xauth
    ```
  - make sure `$DISPLAY` is defined, otherwise:
    ```bash
    sudo sed -i 's/.*X11UseLocalhost.*/X11UseLocalhost no/' /etc/ssh/sshd_config
    sudo systemctl restart sshd
    exit
    ```
- Now reastamblish the `vagrant ssh` connection and it should work.
  - If not, open another terminal while the ssh-connection is opened and hit
    ```bash
    ps -axu | grep ssh
    ```
  - If the ssh-command does not have the options: `ForwardX11=yes -o ForwardX11Trusted=yes`:
    - recheck the above Vagrantfile configuration
  - Otherwise reuse the command with the option `-vvv` to debug the failure.