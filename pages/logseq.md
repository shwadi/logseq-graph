tags:: #[[outliner tool]]

- # Resources
  - Logseq intro course: `https://www.youtube.com/watch?v=oBtKHwFBn0k&list=PLNnZ7rjaL84JjFpgDxRlAOKRa9ie25gtp`
    - quite old; a new one may be out there
  - demo graph by candideu [on github](https://github.com/candideu/Logseq-Demo-Graph?tab=readme-ov-file)
- # Configuration
  - Change spellcheck language:
    - ```bash
      vim ~/.config/Logseq/Preferences
      ```
    - ```
      {"spellcheck":{"dictionaries":["en-US","de"],"dictionary":""}}
      ```
    - restart logseq
    - #+BEGIN_WARNING
      If spellchecking does not work thereafter [[logseq bugs]] #bugs
      #+END_WARNING
      - Check if the desired dicts are loaded into `~/.config/Logseq/Dictionaries/`
        - if not check connectivity (i.e. proxy settings)
  - #+BEGIN_WARNING
    telemetry is active by default
    #+END_WARNING
    - changeable in settings -> advanced -> Send usage data and diagnostics to logseq
    - [Change markdown indentation depth](https://discuss.logseq.com/t/specify-indentation-type-changing-the-default-font/2703/3) if desired (included in dotfiles)
- # Usage
  - Most features are accessible by hitting `<` or `/`
  - Add `#card` tag to add block to the spaced repetition feature accessible by clicking on `Flashcards` on the left sidebar
- # formating quickref
  - ==Highlight with equal sign== and ^^Highlight with carets^^
  - *Italic text* _with underscores_ **bold** ***italic and bold***
  - $$E=mc^2$$
  - This is a {{cloze cloze}}
- # features
  - [macros](https://docs.logseq.com/#/page/macros)
  - [templates](https://docs.logseq.com/#/page/templates)
- # Keybindings quickref
  id:: 65b98d0b-6eb1-43da-8e6f-301cdfc7406e
  - Toggle whether to display brackets in `[[links]]`: `ctrl+c crtl+b`
  - toggle light and dark mode: `t t`
  - move a block above or below other block #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:05:10.190Z
    card-last-reviewed:: 2024-06-05T18:05:10.191Z
    card-last-score:: 5
    - `SHIFT` + `ALT` + `↑`/`↓`
- # Bugs
  - see [[logseq bugs]]
- ## TODO Plugins 2 check
  - https://github.com/vipzhicheng/logseq-plugin-vim-shortcuts
    - https://cheatography.com/bgrolleman/cheat-sheets/logseq-vim-plugin/