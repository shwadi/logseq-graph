- some terminals display titles which can be set like this: `echo -ne "\033]0;MYTITLE\007"`
- usecase
  - `PROMPT_COMMAND='echo -ne "\033]0;${PWD}\007"'` in `~/.bashrc`
    - lets one see the working directory of all panels in [[tmux]] `Choose a window from a list` view