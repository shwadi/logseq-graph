- ways to enter unicode glyph
  - python
    - using escape sequence:
      - `\uxxxx` for up to 4 hex digit number
      - `\Uxxxxxxxx` >4 digit hex number padded to 8 hex digits
      - `\N{name}`
    - using format template #[[python template format]]
      - `'{0:c}'.format(unicode_value)` handy, as the value does not need to be padded or be represented in hex at all
  - vim
    - `:help i_CTRL-V` or `:help i_CTRL-Q`
      - followed by `U<hexnum>`
        - `:help i_CTRL-V_digit` for details
          - use `iabbrev <keyword> <digit>` if digit will be reused multiple times
- ways to determine unicode name
  - pyton
    - via unicodedata
      - ```python
        import unicodedata
        unicodedata.name("😚")
        'KISSING FACE WITH CLOSED EYES'
        ```
- ways to determine unicode number
  - python
    - ```python
      "{0:x}".format(ord("😚"))
      ```
  - vim
    - `:help ga`
- resources
  - http://unicode.org/charts/