- [[script snippet]] to [[escape]] and [[unescape]] [[string]]s
  - utilizing `echo` / `printf`
    - escape:
      - ```bash
        echo $'te\nst' | xargs -0 printf "%q"
        'te'$'\n''st'$'\n'
        ```
        - escapes into POSIX `$''` syntax
        - drawback: [[unix args limitations]]
    - unescape:
      - ```bash
        echo 'te\nst' | xargs -0 echo -en
        ```
        - drawback: [[unix args limitations]]
      - ```bash
        echo "te\nst" | xargs -0 printf "%b"
        ```
        - drawback: [[unix args limitations]]
  - utilizing python
    - escape
      - ```python
        with open("/dev/stdin", "r") as f:
           print(repr(f.read()))
        ```
        - output is rendered in single quotes `'` as soon as the input contains double quotes `"`
          - example: `"te'\"st"` -> `'te\'"st\n'`
            - this quoting style is not usable in shell #[[bash escape single quote within single quoted string]]
      - ```python
        import json
        
        with open("/dev/stdin", "r") as f:
           print(json.dumps(f.read()))
        ```
        - output is rendered in double quotes `"`
    - unescape:
      - ```python
        with open("/dev/stdin", "rb") as f:
           print(f.read().decode('unicode_escape'), end="")
        ```
  - utilizing [[jq]]
    - escape
      - ```bash
        args=$(cat); jq -n "\$args" --arg args "$args"
        ```
    - unescape
      - ```bash
        jq -r .
        ```
- translate acl structure using [[jq]]
  - (a task I head in the past and may refer to in the future)
  - ```bash
    .groups[] |
      select(.acl != null) |
        .entities as $entities |
        .acl | to_entries[] |
          .key as $user |
          .value as $verbs |
          [$user, $verbs, [$entities[]|.name]] |
            .[0] as $eachUser |
            .[1][] as $eachVerb |
            .[2][] as $eachEntity |
            [$eachUser, "can perform", ($eachVerb|ascii_upcase), "on", $eachEntity] | join (" ")
    ```
  - input
    - ```yaml
      groups:
      - name: group1
        acl:
          user1:
          - read
          - write
          user2:
          - read
          - write
        entities:
        - name: entity1
        - name: entity2
      - name: group2
        acl:
          user1:
          - execute
        entities:
        - name: entity3
      - name: group_without_acl
        entities:
        - name: entity4
      ```
  - output (run through `yq . | jq -r ...`)
    - ```
      user1 can perform READ on entity1
      user1 can perform READ on entity2
      user1 can perform WRITE on entity1
      user1 can perform WRITE on entity2
      user2 can perform READ on entity1
      user2 can perform READ on entity2
      user2 can perform WRITE on entity1
      user2 can perform WRITE on entity2
      user1 can perform EXECUTE on entity3
      ```