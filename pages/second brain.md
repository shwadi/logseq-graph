- often referred to as BASB (building a second brain)
- introduced by [[Tiago Forte]] in 2017
- definitions
  - personal knowledge management
  - organization system for personal notes
  - > The build a second brain system is largely the PARA method with the addition of progressive summarization and regular reviews.
    > [...]
    >The BASB system is not exactly a productivity system so there's no particular flow you're supposed to follow to make it work.
    [source](https://workflowy.com/systems/build-a-second-brain/)
- benefits
  - building your own personal wiki