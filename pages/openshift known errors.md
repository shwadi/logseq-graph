## thin-csi pvc creation fails
  - observable event message:
    - > failed to provision volume with StorageClass "thin-csi": rpc error: code = Internal desc = failed to create volume. Error: ServerFaultCode: NotAuthenticated
  - potential solution
    - kill the `vmware-vsphere-csi-driver-webhook` pod in `openshift-cluster-csi-drivers`
      - ```bash
        oc -n openshift-cluster-csi-drivers get po -o name | grep vmware-vsphere-csi-driver-webhook | xargs oc -n openshift-cluster-csi-drivers delete
        ```