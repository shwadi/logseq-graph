tags:: #openshift, #error, #memory, #limit, #procHooks ,#[[write pipe: broken pipe]]

- the following errormessage occures if insufficient memory limit is defined
  - > Error: container create failed: writing syncT "procHooks": write pipe: broken pipe
- steps to reproduce
  - ```bash
    oc run --image=docker.io/ubuntu:18.04 will-crash --dry-run=client -o yaml --command -- sleep 1h | yq -y '.spec.containers[0].resources={limits:{cpu: "5m", memory: "15Mi"}}' | oc create -f -
    ```
  - This error is not 100% reproducible; the pod starts up with this limit in some cases
  - An additional and more meaningful event is being generated if no cpu limit is defined:
    - > Warning  Failed          4m7s                   kubelet            Error: container create failed: time="..." level=error msg="runc create failed: unable to start container process: container init was OOM-killed (memory limit too low?)"