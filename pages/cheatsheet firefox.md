- shortcuts
  - navigation
    - back / forward #card
      card-last-interval:: 108
      card-repeats:: 5
      card-ease-factor:: 3
      card-next-schedule:: 2024-09-21T18:08:23.475Z
      card-last-reviewed:: 2024-06-05T18:08:23.477Z
      card-last-score:: 5
      - Alt + <- / ->
  - Task Manager #card
    card-last-interval:: 33.64
    card-repeats:: 4
    card-ease-factor:: 2.9
    card-next-schedule:: 2024-07-09T09:01:34.865Z
    card-last-reviewed:: 2024-06-05T18:01:34.877Z
    card-last-score:: 5
    - shift + esc
  - screenshot tool #card
    card-last-interval:: 84.1
    card-repeats:: 5
    card-ease-factor:: 2.76
    card-next-schedule:: 2024-08-13T22:14:01.974Z
    card-last-reviewed:: 2024-05-21T20:14:01.974Z
    card-last-score:: 5
    - ctrl + shift + s
  - browser console #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:16:47.740Z
    card-last-reviewed:: 2024-06-05T18:16:47.742Z
    card-last-score:: 5
    - ctrl + shift + j
- configuration quickref
  - change [useragent]: `general.useragent.override`