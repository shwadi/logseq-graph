alias:: git checkout mr locally via alias

- required alias in `~/.gitconfig`:
  - ```
    [alias]
        mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -
    ```
- usage
  - ```bash
    git mr <remotename> <MR_number>
    ```
- [source](https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_troubleshooting.html#check-out-locally-by-adding-a-git-alias)