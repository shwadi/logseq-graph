- Overview of options to generate partial functions
  - example
    - ```python
      def test(a, b, c):
          print(a,b,c)
      ```
  - ## wrapping
    - ```python
      def test(a, b, *, c):
          print(a,b,c)
      
      def test_ab(*args):
          test(*args, c='c')
      ```
  - ## wrapping with lambda
    - ```python
      def test(a, b, *, c):
        print(a,b,c)
      
      test_ab = lambda *args: test(*args, c='c')
      ```
  - ## partial function with keyword parameters
    - ```python
      def test(a, b, *, c):
        print(a,b,c)
      
      from functools import partial
      test_ab = partial(test, c='c')
      ```
  - ## partial function with positional parameters
    - ```python
      def test(c, a, b):
        print(a,b,c)
      
      from functools import partial
      test_ab = partial(test, 'c')
      ```
-