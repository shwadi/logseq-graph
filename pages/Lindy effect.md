tags:: #wisdom
source:: [Wikipedia](https://en.wikipedia.org/wiki/Lindy_effect)

- Theorized phenomenon by which the future [[life expectancy]] of some non-perishable things, like a [[technology]] or an [[idea]], is proportional to their current [[age]].
- proposes:
  - the longer a period something has [[survived]] to exist or be used in the present, the longer its remaining life expectancy