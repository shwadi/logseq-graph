tags:: #packagemanager, #comparison

- | Debian                           | RPM                                                            |
  |----------------------------------|----------------------------------------------------------------|
  | `dpkg -S /path/to/file`          | `rpm -q --whatprovides /path/to/file`                          |
  | `apt-file search <filename>`     | `yum whatprovides '*<filename>'`                               |
  | `apt depends <pkg>`              | `rmp -q --requires <pkg*>`                                     |
  | `apt rdepends <pkg>`             | `rpm -q --whatrequires <pkg>`                                  |
  | `dpkg -L <pkg>`                  | `rpm -ql <pkg*>`                                               |
  | `ls /var/lib/dpkg/info/<pkg>*`   | `rpm -q --scripts <pkg*>`                                      |
  | `apt policy <pkg>`               | `rpm -qi <installed_pkg>`                                      |
  |                                  | `yum info <pkg>`                                               |
  |                                  | `yum [--showduplicates] list <pkg>`                            |
  | `apt update`                     | `yum check-update`                                             |
  | `apt download <pkg>`             | `yumdownloader <pkg>`                                          |
  | list all pkgs in `<repo>`        | `yum --disablerepo="*" --enablerepo="<repo>" list [available]` |
  
  legend:
  * `<pkg*>`: by utilizing `-p` it is possible to reference packages on the filesystem
  * `rpm -q` operates on the installed packages whereas `repoquery` (pkg: `yum-utils`) operates on the referenced repositories