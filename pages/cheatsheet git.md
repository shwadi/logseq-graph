- tracking file changes in git history via `--name-only` option is a bad idea
  - why: renamings will not be rendered
  - solution: use `--name-status` instead
- simple script to build a `Y` git history in order to experiment / explain the git dot notation
  collapsed:: true
  - ```bash
    cd $(mktemp -d)
    git init
    echo common1 >> file
    git add .
    git commit -am common1
    echo common2 >> file
    git commit -am common2
    git checkout -b a
    git checkout -b b
    echo b1 >> file
    git commit -am b1
    echo b2 >> file
    git commit -am b2
    git checkout a
    echo a1 >> file
    git commit -am a1
    echo a2 >> file
    git commit -am a2
    bash
    ```