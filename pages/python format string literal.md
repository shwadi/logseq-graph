alias:: python f-string

- [prefixed string literal](((66048fa3-509f-4e9c-8180-2feb41ed1c48))) with the stringprefix `f` or `F`
  - expressions within `{...}` are evaluated unless they are escaped by doubling them `{{`/`}}`
  - `{expression!conversion:format_spec}`
    - conversion:
      - `!s` calls `str()`
      - `!r` calls `repr()`
      - `!a` calls `ascii()`
    - `format_spec` is interpreted by the `format()` protocol #[[python template format]]
- resources
  - [doc](https://docs.python.org/3.6/reference/lexical_analysis.html#formatted-string-literals)