alias:: impostor phenomenon, impostorism

- > [...]  is a psychological occurrence. Those who have it may doubt their skills, talents, or accomplishments. They may have a persistent internalized fear of being exposed as frauds. Despite external evidence of their competence, those experiencing this phenomenon do not believe they deserve their success or luck.  They may think that they are deceiving others because they feel as if they are not as intelligent as they outwardly portray themselves to be.
  - source: [wikipedia](https://en.wikipedia.org/wiki/Impostor_syndrome)
- opposite of the [[Dunning–Kruger effect]]