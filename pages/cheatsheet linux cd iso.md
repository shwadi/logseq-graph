- folder 2 iso
  - ```bash
    mkisofs -r /path/ > /tmp/new.iso
    ```
    - `-r` does not stand for recursion
- cd 2 iso
  - via `dd`
- mount iso
  - ```bash
    mount -o loop,ro -t iso9660 /path/to/iso /mount/path
    ```
    - `modprobe loop` if mount does not find loop device
- burn iso
  - ```bash
    cdrecord -v speed=12 dev=0,0,0 -data /path/to/iso
    ```