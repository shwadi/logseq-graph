tags:: #[[Hard Times Create Strong Men -- quote]]

- definition
  - > Generation Snowflake is a term used to describe Millennials who exemplify a specific set of traits that set them apart as “snowflakes.”
    >
    > This term was first featured as slang in the 1996 novel [[Fight Club]], authored by [[Chuck Palahniuk]]. As the book explains, “You are not special;
    you are not a beautiful and unique snowflake.”
    >
    > Palahniuk used it as an analogy, but now it’s being used to define an entire generation. Members of “[[generation snowflake]]” are [[easily offended]], triggered by [[microaggression]]s, and concerned with what may or may not be [[politically correct]].
    
    [Source](https://blog.mindvalley.com/snowflake-generation/)