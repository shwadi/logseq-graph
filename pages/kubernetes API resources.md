- core and named api goups
  - ![image.png](../assets/kubernetes_core_and_named_api_groups.png)
    - [source](https://yuminlee2.medium.com/kubernetes-core-and-named-api-groups-d9e7fadb0d17)
- tasks
  - get API resources supported by your cluster
    or
    determine which apiGroup a given resource belongs in
    - ```bash
      kubectl api-resources -o wide
      ```