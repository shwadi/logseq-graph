tags:: #oidc

- thin layer that sits on top of [[OAuth 2.0]] that adds functionality around login & profile information
- deviations from [[OAuth 2.0]]
  - scope `openid` is used
    - [as mentioned here](((65e5f610-0a77-406d-a890-fc0ae381b994)))
  - `🤖 client`  receives an `id token` along with the `🪪 access token` from `📑 authorization server`
    - [as mentioned here](((65e5f6cc-9fe1-4ff9-9dfb-ba618c6b1d5b)))
- differences between [[OAuth 2.0]] and oidc
  - goal
    - [[OAuth 2.0]]
      - enables authorization from one app to another
    - oidc
      - enables a client to establish a  login session && gain information about the `👦 resource owner`