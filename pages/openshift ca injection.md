- ca certificates can be placed in `cm/user-ca-bundle`; key: `ca-bundle.crt` (namespace: `openshift-config`)
  - in previous versions I encountered issues with certain combinations of `empty delimiting lines` and/or `comments`
    - those seam not to be reproducible anymore
- those will be mixed with common system-ca's and injected into arbitrary cm's carrying the label `config.openshift.io/inject-trusted-cabundle=true`
  - all done by the cluster network operator
  - example:
    - oneliner using jq
      - ```bash
        oc create cm <CMNAME> -o json --dry-run=client | jq -y '.metadata+={"labels": {"config.openshift.io/inject-trusted-cabundle": "true"} }' | oc apply -f -
        ```
    - using patch
      - ```bash
        oc create cm <CMNAME>
        oc patch cm <CMNAME> --type=json --patch='
          [
            {
              "op": "add",
              "path": "/metadata/labels",
              "value": {
                "config.openshift.io/inject-trusted-cabundle": "true"
              }
            }
          ]
        '
        ```
- references
  - [docs.openshift.com - Certificate injection using Operators](https://docs.openshift.com/container-platform/4.9/networking/configuring-a-custom-pki.html#certificate-injection-using-operators_configuring-a-custom-pki)