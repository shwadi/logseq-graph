tags:: #vagrant, #bug, #syncfolder

- Tested with
  - image: `generic/debian10`, `generic/debian12`
  - vagrant version: 2.2.19
  - virtualbox:
    - 6.1.38_Ubuntu r153438
    - 6.1.48_Ubuntu r159471
      - ```bash
        dpkg -l | grep virtualbox
        ii  virtualbox           6.1.48-dfsg-1~ubuntu1.22.04.1 ...
        ii  virtualbox-dkms      6.1.48-dfsg-1~ubuntu1.22.04.1 ...
        ii  virtualbox-ext-pack  6.1.48-1~ubuntu1.22.04.1      ...
        ii  virtualbox-qt        6.1.48-dfsg-1~ubuntu1.22.04.1 ...
        ```
- Steps to reproduce:
  - create and enter a syncfolder
  - `ln -s /will_fail/ .`
  - `vagrant ssh` into the vm and list the syncfolder content to make the vm aware of the broken link
  - delete the broken link and create a folder with the same name **on the hypervisor**
  - `'vagrant ssh` into the vm and try to enter the new folder
    - ssh session gets killed
- Script to reproduce:
  - simply execute this script in an empty folder
    - issue is reproducable each time this script is executed
  - ```bash
    #!/usr/bin/env bash
    
    make_vm_aware_of_the_broken_link(){
      # this has not the desired effect:
      # vagrant ssh --tty -c 'bash -lc "cd /vagrant_data; ls --color"'
      # echo "cd /vagrant_data; ls --color" | vagrant ssh
    
      # it is required to run this command directly in the actual tty
      # therefore I'm using this hack:
      vagrant ssh -c 'echo "[ -f /vagrant_data/do_this ] && . /vagrant_data/do_this" >> ~/.bashrc'
      echo "cd /vagrant_data; ls; exit" > do_this
      vagrant ssh
      rm do_this
    }
    
    cat << EOF > Vagrantfile
    Vagrant.configure("2") do |config|
      config.vm.box = "generic/debian10"
      config.vm.synced_folder ".", "/vagrant_data"
    end
    EOF
    
    rm -rf will_* #cleanup
    
    RANDOM_TAIL=$(mktemp -u XXX)
    
    ln -s /will_fail_$RANDOM_TAIL/ .
    vagrant up
    
    make_vm_aware_of_the_broken_link
    
    ln -s /will_not_fail_$RANDOM_TAIL/ .
    
    for f in will_*; do
      rm $f
      mkdir $f
    done
    
    vagrant ssh -c "ls /vagrant_data/will_not_fail_$RANDOM_TAIL/ && echo this works"
    vagrant ssh -c "ls /vagrant_data/will_fail_$RANDOM_TAIL/ && echo 'this works too?!?' || echo this does not work"
    ```