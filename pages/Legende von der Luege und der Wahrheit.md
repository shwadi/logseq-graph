tags:: #Gesellschaftskritik, #Lüge, #Wahrheit
source:: https://austenweb.de/die-legende-von-der-wahrheit-und-der-luege/

- > Laut einer Legende aus dem 19. Jahrhundert treffen sich die Wahrheit und die Lüge eines Tages.
  >
  > Die Lüge sagt zur Wahrheit: “Heute ist ein wunderbarer Tag”! Die Wahrheit blickt in den Himmel und seufzt, denn der Tag war wirklich schön.
  >
  > Sie verbringen viel Zeit miteinander und kommen schließlich neben einem Brunnen an.
  >
  > Die Lüge erzählt die Wahrheit: “Das Wasser ist sehr schön, lass uns zusammen baden!” Die Wahrheit, erneut verdächtig, testet das Wasser und entdeckt, dass es wirklich sehr nett ist. Sie ziehen sich aus und beginnen zu baden.
  >
  > Plötzlich kommt die Lüge aus dem Wasser, zieht die Kleider der Wahrheit an und rennt davon. Die wütende Wahrheit kommt aus dem Brunnen und rennt überall hin, um die Lüge zu finden und ihre Kleidung zurückzubekommen.
  >
  > Die Welt, die die Wahrheit nackt sieht, wendet ihren Blick mit Verachtung und Wut ab.
  >
  > Die arme Wahrheit kehrt zum Brunnen zurück und verschwindet für immer und versteckt darin ihre Scham. Seither reist die Lüge um die Welt, verkleidet als die Wahrheit, befriedigt die Bedürfnisse der Gesellschaft, denn die Welt hat auf keinen Fall den Wunsch, der nackten Wahrheit zu begegnen.