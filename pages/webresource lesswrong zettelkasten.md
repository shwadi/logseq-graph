source:: https://www.lesswrong.com/posts/NfdHG6oHBJ8Qxc26s/the-zettelkasten-method-1
publishing-date:: 2019-09-20
tags:: #[[note taking]], #Zettelkasten

- ## summary
  - the author advises to start with a physical Zettelkasten to gain experience before eventually switching to a digital version
    - in fact this is the primary focus of this article
      - besides that the author describes his note-taking journey with the proprietary [[outliner]] tools [[workflowy]] and [[dynalist]]
    - there is a lot of discussion on what product to buy, what paper size to choose etc.
  - it might be a good idea to start the Zettelkasten process either ...
    - with an unclear idea one is struggling to articulate
      - to experience the advantage of the Zettelkasten process (in case it is suitable for oneself)
    - or an totaly irrelevant idea
      - > to make sure you don’t feel like everything has to be nicely organized and highly significant.
- ## quotes
  - zettelkasten-related
    - > I mainly read *How to Take Smart Notes*, which is the best book on Zettelkasten as far as I know -- it claims to be the best write-up available *in English*, anyway.
    - > **I strongly recommend trying out Zettelkasten on actual note-cards,** even if you end up implementing it on a computer.
    - > A Zettelkasten is supposed to be one repository for everything -- you’re not supposed to start a new one for a new project, for example.
      > But, I have several Zettelkasten, to test out different formats: different sizes of card, different binders.
      > [...]
      > For example, my main 3x5 Zettelkasten is “S” (for “small”).
      > I have another Zettelkasten which is “M”, and also an “L”. When referencing card 1.1a within S, I just call it 1.1a.
      > If I want to refer to it from a card in M, I call it S1.1a instead.
      > And so on.
      >
      Apparently Luhmann [did something similar](https://zettelkasten.de/posts/luhmanns-second-zettelkasten/), starting a new Zettelkasten which occasionally referred to his first.
  - note-taking related
    - > Another source on note-taking which I recommend highly is Lion Kimbro’s *How to Make a Complete Map of Every Thought You Think ([html](https://users.speakeasy.net/~lion/nb/html/),* *[pdf](https://users.speakeasy.net/~lion/nb/book.pdf)).*
      > [...]
      > it contains a wealth of inspiring ideas about note-taking systems, including valuable tips for the raw physical aspects of keeping paper notes.
      > I recommend reading [this interview with Lion Kimbro](https://gilest.org/lion-kimbro.html) as a “teaser” for the book -- he mentions some things which he didn’t in the actual book, and it serves somewhat as “the missing introduction” to the book.
      > (You can skip the part at the end about wikis if you don’t find it interesting; it is sort of outdated speculation about the future of the web, and it doesn’t get back to talking about the book.)
    - > I had realized that locking everything into a unified tree structure, while good for the purpose of slowly improving a large ontology which organized a lot of little thoughts, was keeping me from just writing whatever I was thinking about.