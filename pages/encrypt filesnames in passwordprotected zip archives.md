tags:: #[[header encryption]], #archive

- not possible with `zip`
  - unless using nested archives
    - ```bash
      zip -0 -r directory.zip /path/to/directory
      zip -e -n : encrypted.zip directory.zip
      ```
      - `-0` -> no compression
      - `-n :` -> attempt compression on all files
- possible with `7z`
  - ```bash
    7z a -p[optionalPW] -mhe=on archive.7z /path/to/directory
    ```
    - `-mhe=on` -> Enables archive header encryption
    - `[optionalPW]` -> is optional