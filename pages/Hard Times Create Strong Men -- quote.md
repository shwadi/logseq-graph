tags:: #quote, #wisdom, #Gesellschaftskritik

- English quote:
  - > Hard Times Create Strong Men, Strong Men Create Good Times, Good Times Create Weak Men, Weak Men Create Hard Times.
- German quote:
  - > Harte Zeiten schaffen starke Männer. Starke Männer schaffen gute Zeiten. Gute Zeiten schaffen schwache Männ:innen. Und schwache Männ*innen schaffen harte Zeiten.
- This famous quote describes the state of our society in an epic way.
- If I would be asked to choose a quote for the imprint of the [[tombstone]] for our [[degenerative]] [[society]], this would be the one!