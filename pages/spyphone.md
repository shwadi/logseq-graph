tags:: #Gesellschaftskritik

- I use the term spyphone to refer to a closed source device loaded with a huge batch of sensors each of which are shadowing the life of the consumers who are happily carrying it around 24/7.
- The term smartphone is from my point of view misleading as the smartness lies not in the phone. It lies in the companies who managed to place their wiretap devices into nearly every pocket and who even managed to let the consumer ...
  - pay money for living a life in a [[digital panopticon]]
  - be happy to "own" such a device not realizing that you don't own anything if you are not allowed to inspect/change/control it #[[open source]]
  - buy new versions of this devices in a Pavlov manner, each time the marketing bell rings or the [[planed obsolescence]] forces them to do so #[[Pavlov's Dog]]