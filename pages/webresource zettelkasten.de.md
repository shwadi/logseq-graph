query-sort-by:: page
query-sort-desc:: false
#+BEGIN_QUERY
{
:title [:h2 "🗒 local notes"]
:query [
       :find (pull ?p [*])
       :where
       [?p :block/properties ?prop]
       [(get ?prop :source) ?source]
       [(clojure.string/starts-with? ?source "https://zettelkasten.de")]
      ]
}
#+END_QUERY
