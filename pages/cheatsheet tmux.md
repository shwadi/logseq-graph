- shortcuts
  - break pane to a new window #card
    card-last-interval:: 97.56
    card-repeats:: 5
    card-ease-factor:: 2.9
    card-next-schedule:: 2024-08-20T10:29:04.236Z
    card-last-reviewed:: 2024-05-14T21:29:04.237Z
    card-last-score:: 5
    - `c-b !`
    - how to put it back: ((65d1e487-6a7e-4c1f-9499-5396cb917953))
  - list all paste buffers #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:04:59.903Z
    card-last-reviewed:: 2024-06-05T18:04:59.904Z
    card-last-score:: 5
    - `c-b #`
    - `c-b =` lets one actually select one which is way more useful
  - kill current window #card
    card-last-interval:: 97.56
    card-repeats:: 5
    card-ease-factor:: 2.9
    card-next-schedule:: 2024-08-20T10:29:58.916Z
    card-last-reviewed:: 2024-05-14T21:29:58.917Z
    card-last-score:: 5
    - `c-b &`
  - prompt for window index to select #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:09:35.699Z
    card-last-reviewed:: 2024-06-05T18:09:35.700Z
    card-last-score:: 5
    - `c-b '`
  - move the current window (=change window index) #card
    card-last-interval:: 31.36
    card-repeats:: 4
    card-ease-factor:: 2.8
    card-next-schedule:: 2024-06-11T04:40:22.921Z
    card-last-reviewed:: 2024-05-10T20:40:22.923Z
    card-last-score:: 5
    - `c-b .`
  - jump to the previously active pane #card
    card-last-score:: 5
    card-repeats:: 5
    card-next-schedule:: 2024-09-21T18:17:09.586Z
    card-last-interval:: 108
    card-ease-factor:: 3
    card-last-reviewed:: 2024-06-05T18:17:09.588Z
    - `c-b ;`
  - choose a paste buffer from a list #card
    card-last-interval:: 97.56
    card-repeats:: 5
    card-ease-factor:: 2.9
    card-next-schedule:: 2024-08-20T10:29:46.309Z
    card-last-reviewed:: 2024-05-14T21:29:46.309Z
    card-last-score:: 5
    - `c-b =`
  - choose a window from a list #card
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:04:25.683Z
    card-last-reviewed:: 2024-06-05T18:04:25.684Z
    card-last-score:: 5
    - `c-b w`
- tasks
  - reorganize pane #card
    id:: 65d1e487-6a7e-4c1f-9499-5396cb917953
    card-last-interval:: 108
    card-repeats:: 5
    card-ease-factor:: 3
    card-next-schedule:: 2024-09-21T18:16:01.349Z
    card-last-reviewed:: 2024-06-05T18:16:01.351Z
    card-last-score:: 5
    - open a new helper pane on the desired position
    - mark it via `c-b m`
    - navigate to the pane you want to replace and hit `:swap-pane`
    - delete the helper pane
  - send keys to multiple panes #card
    card-last-interval:: 3.71
    card-repeats:: 1
    card-ease-factor:: 2.36
    card-next-schedule:: 2024-06-12T11:12:25.163Z
    card-last-reviewed:: 2024-06-08T18:12:25.163Z
    card-last-score:: 3
    - `setw synchronize-panes on`
    - ways to unsync a certain panes
      - open a clock (`<prefix> t`)
      - run `:select-pane -d` (to reenable `-e`)
  -