- avoid **-a** and **-o** binary primaries
  - > The XSI extensions specifying the **-a** and **-o** binary primaries and the '(' and ')' operators have been marked obsolescent. (Many expressions using them are ambiguously defined by the grammar depending on the specific expressions being evaluated.)
    > [...]
    > Tests that require multiple *test* operations can be done at the shell level using individual invocations of the *test* command and shell logicals, rather than using the error-prone **-o** flag of *test*.
    [source](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/test.html)
  - `-a` and `-o` always evaluates both sides
    - example
      - ```bash
        i=none
        [ $i != none -a $i -gt 0 ]
        -bash: [: none: integer expression expected
        ```
      -