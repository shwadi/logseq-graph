source:: https://zettelkasten.de/introduction/

- summary
  - Interesting article showing the potential way of structuring hyperlinked notes with basic text editors. If I didn't stumble upon [[logseq]] first, I would have definitely done it this way.
- quotes and thoughts
  - > A Zettelkasten is a personal tool for thinking and writing. It has hypertextual features to make a web of thought possible. The difference to other systems is that you create a web of thoughts instead of notes of arbitrary size and form, and emphasize connection, not a collection.
  - The term Zettelkasten was introduced by the social scientist [[Niklas Luhmann]], who published 50 books and over 600 articles (and had a lot of unpublished work) by utilizing a physically hyperlinked Zettelkasten utilizing the [[Luhmann’s numbering system]].
  - > The Zettelkasten Method is an organic and non-linear, even living, approach on note-taking.
  - > The difference between regular note-taking systems and a Zettelkasten is the emphasis on forming relationships. *A Zettelkasten makes connecting and not collecting a priority*.
  - principle of atomicity:
    - The base unit of a Zettelkasten is a thought
    - In a book a thought may be spread over the whole book or some pages/chapters. It may not be uniquely/directly adressable, because a book is not a web of thought.
    - Same is true for encyclopedias, in which one can address a page/paragraph but not a thought.
      - Zettelkasten is a tool of thoughts; encyclopedias a tool for information retrieval
  - [[privacy]] vs publicity
    - > If you don’t keep your diary absolutely private, you wouldn’t write some things down, and you’d filter other things, therefore distorting them. Writing for yourself is and should be different from writing for the public.
      >
      > That does not mean that it is never useful to create a shared, project-specific hypertext. But that is not what we are talking about when we talk about a Zettelkasten.
      - I wonder if capturing those thoughts within a [[proprietary]] app on a equally proprietary [[spyphone]] or backing it up in the [[cloud]] storage of companies who openly admit that their business model are based on the gathering and selling of data is counting as "private" #irony #Gesellschaftskritik
      - But this argument makes sense thus this repo is not and will not become a [[Zettelkasten]] in the form described here #[[about this graph]]
  - One should always put effort into a note like
    - rewrite it in own words
      - Quoting is fine but own toughs should be added below
    - translate information to knowledge by adding context and relevance
  - Interesting question raised by a forum user which is quoted in the article:
    - > I sometimes struggle to determine whether it is worth writing a detailed Zettel about what makes the website worthwhile to write a Zettel about, but I don’t want to write for the sake of writing, and most of the time I am not sure if it is just transient stuff or whether it really is useful to keep around long term and not just for the processing of a particular thought/query I had. I don’t want to turn my Zettelkasten into busywork, i.e. a kind of procrastination.
    - The author answers that one never knows if an information will be useful in the future thus it is better to in doubt capture it.
  - > To create Zettels about the relationship between other Zettels, is called a Structure Note. The practice of creating Structure Note will further train your ability to deal with general patterns of knowledge.