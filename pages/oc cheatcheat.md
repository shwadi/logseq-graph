tags:: #openshift

- execute command attached to tty on a new image instance
  - ```bash
    oc run [...] --rm -it --restart=Never --command -- <cmd> <arg1> ... <argN>
    ```
    - If command is shortliving `--restart=Never` is required; otherwise the pod would fall into [[CrashLoopBackOff]] and the oc command would terminate without presenting the `<cmd>` output