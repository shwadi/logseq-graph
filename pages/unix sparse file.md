- create
  - create new
    - ```bash
      dd if=/dev/zero of=$DESIRED_LOCATION bs=1 count=0 seek=$DESIRED_SIZE
      ```
  - convert existing file
    - `fallocate` (debian `util-linux` pkg)
      - ```bash
        fallocate --dig-holes $FILE_OF_INTEREST
        ```
- discussion
  - advantage
    - Storage is only allocated if needed
  - disadvantage
    - file may become fragmented
    - if copied by program not supporting sparse files, the copied version will be a regular file