- ```bash
  openssl s_client -connect example.com:443
  ```
  - common appending to filter for the certificate:
    - ```bash
      <<<"" 2>/dev/null | sed -n '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
      ```
      - yes, `<<<""` is required to terminate the connection, kept opened by `s_client`
    - note: this filtering is not required for `openssl x509 -noout -text`
  - `-showcerts` shows the entire certificate chain in PEM format as well