source:: unknown
tags: #quote, #greatness, #permission

- > If you want to achieve greatness, stop asking for permission.
- related to [[quote forgiveness vs permission]]