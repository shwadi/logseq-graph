- array
  - expand array
    - `"${arr[@]}"` || `"${arr[*]}"`
      - if unquoted: results undefined
      - `$*` merges args into one argument `1x2x...` where x is the first char of `IFS`
      - `$@` expands each element as a separate argument
- `NULL` byte
  - pitfalls
    - can't store `NULL` byte in variables (encoding required)
    - can't pass strings with `NULL`byte as args to programs
      - args are passed as c-like char-arrays, in wich `NULL` byte is an end-indicator
      - this is the reason why this don't work:
        - ```bash
          echo $'a\0b'
          ```
    - gets eaten up by subshells
      - ```bash
        xxd <<< $(head -c1 /dev/zero)
        -bash: warning: command substitution: ignored null byte in input
        00000000: 0a                                       .
        ```
- workarounds
  - for [[alias]] specifications to be interpreted by [[bash]], it needs to be launched in interactive mode `-i`