tags:: #jq, #yq, #slurp

- the task
  - suppose you have managed to fumble some desired elements by a rather long pipe query and do now want to combine everything into one final array
    - example
      - ```bash
        cat << EOF | yq -y '.items[]|.subitem[0]|.desired_element[]'
        items:
        - subitem:
          - desired_element:
            - l1.1
            - l1.2
        - subitem:
          - desired_element:
            - l2.1
            - l2.2
        EOF
        ```
      - what you get:
        - ```yaml
          l1.1
          --- l1.2
          --- l2.1
          --- l2.2
          ```
      - what you want:
        - ```yaml
          [
            "l1.1",
            "l1.2",
            "l2.1",
            "l2.2"
          ]
          ```
- the problem
  - if ...
    - you are rather inexperienced with `jq`/`yq`
    - your are in a shell-like coding/thinking flow
    - the query is a rather log oneliner (the solution is obvious otherwise)
  - you may be tempting to ...
    - search for the solution at the end of the pipe, like surrounding `.disired_element[]` with `[]`
    - search for a function one could pipe the final result to, which would than collect everything into one array (shell-like thinking)
- the solution
  - one solution is to pipe the output to another `jq`/`yq` instance, launched with the `slurb` option: `yq -s .`
  - another solution is to place the opening `[` further to the left (in the above example to the very beginning)