tags:: snippet

- ```bash
  charset='[:graph:]' # all printable chars
  charset='[:alpha:][:digit:]._-' # alphanumeric plus ._-
  secretlength=80
  tr -dc "$charset" < /dev/urandom | fold -w ${1:-$secretlength} | head -1
  ```