source:: https://zettelkasten.de/posts/building-a-second-brain-and-zettelkasten/
tags:: #Zettelkasten, #[[second brain]]

- abbreviations used
  - BASB: building a second brain
  - ZKM: Zettelkasten method
- summary quotes
  - > Simplified, BASB is a source material feeder system for project-oriented
     self-organization. It is especially suitable for people whose projects
    are particularly dependent on source material. Oddly enough, the
    processing of knowledge seems almost to be considered a necessary evil,
    to be automated and simplified as much as possible.
  - > BASB speaks the language of action. ZKM speaks the language of knowledge.
    >
    > The basic categories of BASB are importance and urgency. These are categories of action.
    > The basic categories of ZKM are atomic thought and its relation to other thoughts.
    >
    > BASB chooses as its filing categories PARA, a folder system that is a hierarchy of urgency.
    > ZKM is a heterarchy of thoughts that can float freely in the ether like in the Platonic world of ideas.
  - > BASB is a hybrid of (1) information management system and (2)
    project management system. The Zettelkasten Method is a system of
    creating an integrated thinking environment and working with it.
  - > BASB and the ZKM are two completely different approaches to working. But their differences don’t come from the fact that they solve the same problem differently, but that they address different parts of the knowledge-based value chain. This makes them not only easily compatible. In fact, I think it is advisable to combine them. That way, you get the best of the worlds of productivity-focused self-organization and deep understanding-focused integrated thinking environments.
- > BASB is for people who want to manage their information streams and
  resources in a project-oriented way. But probably the most important
  target group is people who feel overwhelmed by uncertain tasks and
  modern information overload.
- interesting thoughts on people who tend to overwhelm them self with information
  - >[...] These are the ones who get lost in analyzing and constructing a perfect
    system. They run the risk of doing more work on their system than using
    it for its intended purpose. They can become overwhelmed by a
    self-created information overload as they build an elaborate system of
    RSS feeds, web clippers, and other techniques of capturing information
    and information resources. But they also run the risk of conducting
    purposeless research, neglecting project- and deadline-oriented work.
- > Don’t worry about analyzing, interpreting, or categorizing each point to
   decide wether to highlight it. That is way too taxing and will break
  the flow of your concentration. Instead, rely on your intuition to tell
  you when a passage is interesting, counterintuitive, or relevant to your
   favorite problems or current project.
  - The author argues against this quote as this work needs to be done anyway and which is missing in the BASB process.
- > Forte recommends relying on system-wide search.[59](https://zettelkasten.de/posts/building-a-second-brain-and-zettelkasten/#fn59)
    But my experience with the Zettelkasten Method has shown me that as
  the size of the Zettelkasten increases, the search function becomes less
   reliable and more cumbersome to use. I’m already at the point where the
   search function has lost much of its usefulness. My Zettelkasten is
  simply too complex for the search to provide reliable access.
  - damn, this guy is crazy...
