tags:: #ansible, #module

- listing modules
  - Determine installation path of the default modules by querying the docs of one of them (i.e.: `groups`)
    - ```bash
      strace -fo /dev/stdout ansible-doc group | grep openat.*group
      770560 openat(AT_FDCWD, "/usr/lib/python3/dist-packages/ansible/inventory/__pycache__/group.cpython-310.pyc", O_RDONLY|O_CLOEXEC) = 3
      770560 openat(AT_FDCWD, "/usr/lib/python3/dist-packages/ansible/modules/group.py", O_RDONLY|O_CLOEXEC) = 3
      ```
      - another way would be to utilize the [[packagemanager]]
    - In the above case all default modules are placed in: `/usr/lib/python3/dist-packages/ansible/modules/`
- notes on default modules:
  - `command`:
    - default module for ad-hoc commands but lacks possibility to use pipelines, which is supported by the `shell` module