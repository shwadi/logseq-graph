- by default it is not possible to debug an infra node on a tainted node with `NoExecute` effect
- workaround:
  - ```bash
    oc new-project dummy
    $ oc patch namespace dummy --type=merge -p '{"metadata": {"annotations": { "scheduler.alpha.kubernetes.io/defaultTolerations": "[{\"operator\": \"Exists\"}]"}}}'
    $ oc debug node/... --to-namespace dummy
    ```
    - [Source](https://access.redhat.com/solutions/4976641)