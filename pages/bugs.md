# Snapd
  - applications launch slowly
    - Identification:
      - ```bash
        snap run --debug-log <app>
        # complains about document portal
        
        systemctl --user status xdg-document-portal.service
        # complains corrupt flatpack-db
        ```
    - Solution
      - ```bash
        mv .local/share/flatpak{,_old}
        ```
