source:: quoted in the U.S. Navy's *Chips Ahoy* magazine (July 1986)
tags:: #funny, #quote, #forgiveness, #permission

- > It's easier to ask forgiveness than it is to get permission.