tags:: #quote, #[[Carl J. Burckhardt]], #ignorant, #denken, #Irrtum
source:: https://yoice.net/carl-j-burckhardt-es-gehoert-zum-schwierigsten

- English quote:
  - > It is one of the most difficult things that can be imposed on a thinking man, to have to witness the course of a historical process among ignorant people, whose inevitable outcome he has long known with clarity. The time of the error of others, of false hopes, of blindly committed mistakes then becomes very long.
- German quote:
  - > Es gehört zum Schwierigsten, was einem denkenden Menschen auferlegt werden kann, wissend unter Unwissenden den Ablauf eines historischen Prozesses miterleben zu müssen, dessen unausweichlichen Ausgang er längst mit Deutlichkeit kennt. Die Zeit des Irrtums der anderen, der falschen Hoffnungen, der blind begangenen Fehler wird dann sehr lang.
- reminds me of [[Hard Times Create Strong Men -- quote]]