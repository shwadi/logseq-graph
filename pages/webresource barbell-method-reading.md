source:: https://zettelkasten.de/posts/barbell-method-reading/
tags:: #book, #reading, #[[barbell method reading]]

- summary
  - > Read thoroughly and invest more time (risky investment) when necessary, stick to quickly skimming (low risk) for the rest. [source](https://zettelkasten.de/posts/overview/)
- The author argues against [[speed reading]] techniques as they either decrease comprehension for the sake of speed or even involve skipping large parts of the text.
  - the sheer amount of input is too much to digest
  - qualitative reading requires time and effort to let new ideas sink in
    - one should not waste a lot of time with mediocre books
- The barbel method foresees to read a book twice:
  - first time marking the relevant passages
  - second time collecting the passages and working upon them
- The author does not stick with this method in [some cases](https://zettelkasten.de/de/posts/erfahrungsbericht-7-atomic-habits/)
- The author mentions the [[book Deep Work by Cal Newport]] which presents little novel ideas but does it in an inspiring way
  id:: 65ba47eb-eae6-4d58-b461-d52ba7cf37cb
- quotes
  - > The [[Barbell Method]] is a phrase coined by [[Nassim Taleb]]. It means that you make sure that the majority of your investment is safe while you make small but very risky bets.
  - > Some people are impressed if someone reads three or four books a week. They think “Wow, he surely knows a lot.” I don’t share this opinion. To me, it is just a testimony of their shallowness in processing. This is fine if you don’t care. If you read for fun, go ahead and do it. But don’t try to impress anyone with things you don’t have invested energy into.