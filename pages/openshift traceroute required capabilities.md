- if traceroute exits with
  - > socket(AF_INET,3,1): Operation not permitted
- chances are the `NET_RAW` capability is missing
- working example
  - ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: alpine
    spec:
      containers:
      - image: alpine
        name: alpine
        command:
         - traceroute
         - test.de
        securityContext:
          capabilities :
            add:
            - NET_RAW
      restartPolicy: Always
    ```
  - tested with final cap collection:
    - > cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw=ep