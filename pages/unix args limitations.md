- This topic need further investigation
- evaluation
  - ```bash
    LANG=C xargs --show-limits
    Your environment variables take up 3192 bytes
    POSIX upper limit on argument length (this system): 2091912
    POSIX smallest allowable upper limit on argument length (all systems): 4096
    Maximum length of command we could actually use: 2088720
    Size of command buffer we are actually using: 131072
    Maximum parallelism (--max-procs must be no greater): 2147483647
    ```
  - ```bash
    getconf ARG_MAX
    2097152
    ```