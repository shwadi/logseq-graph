- `oc apply` will store the applied configuration in the `kubectl.kubernetes.io/last-applied-configuration` #annotation of the according object
  - unless launched with `--server-side` option
  - unless it extends the 262144 bytes allowed annotation size
- the content of this annotation influences the behavior of future `oc apply` runs
- example
  - ```bash
    oc delete cm test || true
    oc create cm test --from-literal test=test
    oc annotate cm test new-annotation=test
    echo first diff:
    oc create cm test --from-literal test=test --dry-run=client -o yaml | oc diff -f -
    echo ---
    oc get cm test -o yaml | oc apply -f -
    echo second diff:
    oc create cm test --from-literal test=test --dry-run=client -o yaml | oc diff -f -
    echo ---
    oc annotate cm test kubectl.kubernetes.io/last-applied-configuration-
    echo third diff:
    oc create cm test --from-literal test=test --dry-run=client -o yaml | oc diff -f -
    echo ---
    ```
    - the first diff will be empty but the second will show that the same command would delete the annotation `new-annotation`, which was added in line `3`
    - this is because the apply run in line `7` marked it as explicitly added
      - thus the avoidance of its specification in future apply executions will be interpreted as an deletion attempt
    - the third diff is once again empty, because of the #[[kubernetes annotation removal]] in line `11`