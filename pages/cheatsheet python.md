- create a dictionary containing the current scope's local variables #card
  card-last-interval:: 108
  card-repeats:: 5
  card-ease-factor:: 3
  card-next-schedule:: 2024-09-21T18:09:15.817Z
  card-last-reviewed:: 2024-06-05T18:09:15.819Z
  card-last-score:: 5
  - `locals()`
- tuples
  - what is the term for this operation: #card
    card-last-interval:: 31.36
    card-repeats:: 4
    card-ease-factor:: 2.8
    card-next-schedule:: 2024-06-11T04:38:20.478Z
    card-last-reviewed:: 2024-05-10T20:38:20.479Z
    card-last-score:: 5
    ```python
    a, b, c = tuple_var
    ```
    - multiple assignment
  - How is the technique called, which enables to split **logical lines** over multiple **physical lines**?
    - **explicit line joining**: `\`
    - **implicit line joining**: `()`
  - What is the terminology for `b"..."`, `f"..."`, `r"..."` #card
    card-last-interval:: 56.69
    card-repeats:: 5
    card-ease-factor:: 2.42
    card-next-schedule:: 2024-08-04T10:15:07.034Z
    card-last-reviewed:: 2024-06-08T18:15:07.036Z
    card-last-score:: 3
    - [doc](https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals)
    - common terminology:
      - `★"..."`: prefixed string literal
        id:: 66048fa3-509f-4e9c-8180-2feb41ed1c48
      - `b/f/...`: stringprefix
    - `b"..."` / `B"..."`:
      - `bytes literal`
      - `b/B`: `bytesprefix`
      - creates a `bytes object`
      - [doc](https://docs.python.org/3/reference/lexical_analysis.html#string-and-bytes-literals)
    - `f"...` / `F"..."`:
      id:: 660485fe-1560-4eab-83fd-dc5ca80c9254
      - [formatted string literal]([[python format string literal]]) or [f-string]([[python f-string]])
    - `r"..."` / `R"..."`
      - `...` is (treated as) a `raw string`
    - `u"..."` / `U"..."`:
      - `...` is treated as `unicode string` in python2
      - unneeded in python3
- how to force keyword-only arguments on function usage #card
  card-last-interval:: 33.64
  card-repeats:: 4
  card-ease-factor:: 2.9
  card-next-schedule:: 2024-06-15T09:25:28.254Z
  card-last-reviewed:: 2024-05-12T18:25:28.254Z
  card-last-score:: 5
  - with the `*` **separator**:
    - ```python
      def test(a, *, b, c):
        print(a,b,c)
      ```
      - this allows only `a` to be defines as positional arg
- how to copy a list #card
  card-last-interval:: 33.64
  card-repeats:: 4
  card-ease-factor:: 2.9
  card-next-schedule:: 2024-06-22T09:42:29.377Z
  card-last-reviewed:: 2024-05-19T18:42:29.377Z
  card-last-score:: 5
  - i.e. by using the slice operator: `new_list = list[:]`