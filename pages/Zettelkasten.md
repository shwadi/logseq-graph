alias:: slip box

- seams to be often referred to as [[web of notes]]
- definition
  - collection of [[note]]s (thus [[thought]]s) on various topics
  - tech backed system extending the [[human memory]] which is struggling to ...
    - hold many [[thought]]s in memory
    - recall all related old [[thought]]s when new one arise
- advantage
  - recall ideas to recognize relationships and thereby ...
    - create new ideas
    - improve depth of understanding
    - recognize knowledge holes
    - improve capacity for remembering as our [[brain]] struggles to hold [[isolated facts]]
- resource
  - [[webresource zettelkasten.de]]