- ansibe will return a `job_id` for jobs launched in background which can be used to query the status via:
  - ```bash
    ansible <TARGET> -m async_status -a "jid=<JOB_ID>"
    ```
  - checking `~/.ansible_async/<JOB_ID>` on the remote server
    id:: 65ba94ba-6cb9-479e-9473-fd400f07c1a5
- The following options / playbook parameters are relevant to run jobs in background:
  - `-B` (parameter: `async`): maximum amount of seconds to let the job run
  - `-P` (parameter: `poll`): amount of seconds to wait between polling the servers for an updated job status
    - if set to 0 it becomes a [[fire and forget]] job (in this case `-B` should of course be set fairly high)
      - status is still query-able by ((65ba94ba-6cb9-479e-9473-fd400f07c1a5))