alias:: Propaganda
tags:: #[[mass media]]

- # Preamble: Please [[snowflake]], do not feel offended by this topic!
  - Regardless on where in the world you are reading this or what government you are committed to: propaganda has the strange ability to always be performed somewhere else; mostly in countries you have very little and/or very biased knowledge of and no influence upon. Strangely this are most often the countries you and your most trusted media dislike. Thus, **of course**, this topic has no relation to the media you trust and love so much. #Gesellschaftskritik #irony
- one of the key components of [[propaganda]] is to [[fragment information]] down to the level of [[single events in time]] creating an [[illusion of beeng informed]] #[[isolated facts]]
  - by repeating the fragments supporting your [[narrative]] and suppressing or just limiting the ones opposing it, one can easily manipulate the [[mind of the masses]] without the necessity to [[lie]] #truth
    - 🇩🇪 dies ist der Grund, wieso [[Lügenpresse]] ein sehr unpassender Begriff ist; [[Lückenpresse]] trifft es besser