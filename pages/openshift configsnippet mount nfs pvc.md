- [[openshift]] [[configsnippet]] to [[mount]] [[nfs]] [[pvc]]
  - ```yaml
    apiVersion: v1
    kind: PersistentVolumeClaim
    metadata:
      name: nfspvc
    spec:
      accessModes:
      - ReadWriteMany
      resources:
        requests:
          storage: 1Gi
      volumeMode: Filesystem
    ---
    apiVersion: v1
    kind: Pod
    metadata:
      name: nfswriter
    spec:
      volumes:
      - name: nfsvolume
        persistentVolumeClaim:
          claimName: nfspvc
      containers:
      - name: nfswriter
        image: docker.io/alpine
        command:
        - sh
        - -c
        - |
          echo -n "Mounted pvc: "
          mount | grep /mnt | cut -d ' ' -f 1 | rev | cut -d '/' -f 1 | rev
        volumeMounts:
        - mountPath: /mnt
          name: nfsvolume
      dnsPolicy: ClusterFirst
      restartPolicy: Never
    ```