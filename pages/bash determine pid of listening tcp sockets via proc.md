- quick and dirty solution for systems without `ss` or `netstat`
- ```bash
  all_inodes=$(2>/dev/null find /proc/*/fd -type l | 2>/dev/null xargs ls -l | grep socket | sed -E 's/ +/ /g' | cut -d ' ' -f 9-)
  
  while read inputline; do
      <<< "$inputline" cut -d : -f 3 | cut -d ' ' -f 1 | xargs  -I% bash -c 'echo -n $((16#%))'
      echo -n ' --> '
      inode=$(sed -E 's/ +/ /g' <<< "$inputline" | cut -d ' ' -f 10)
      grep -F "[${inode}]" <<< "$all_inodes" | tr '\n' ' '
      echo
  done <<< "$(sed 1d /proc/net/tcp)"
  ```