- I encountered a few times `vagrant up` to hang on this message:
  - > SSH auth method: private key
  - One need to pay attention to the ssh port which is clamed to be used during startup:
    - > 22 (guest) => 2222 (host) (adapter 1)
    - in the above case the `2222` port was already in use by another vagrant vm
      - ```bash
        sudo ss -tlnp | grep 2222 | grep -o 'pid=[0-9]*' | cut -d = -f 2 | xargs ps
        ```
      - shutting the other vm down solved the issue
      - it is unclear why this port mismatch occurred