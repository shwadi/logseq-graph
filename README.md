# logseq graph

This is my public logseq graph which is and will always be work in progress and incomplete.

Check [about this graph](https://shwadi.gitlab.io/logseq-graph/#/page/about%20this%20graph) for more info.

If your browser has trouble rendering it, visit [pages/about this graph.md](pages/about this graph.md) instead or [check the workaround described next](#browser-issues).

## Browser issues

In combination with firefox and [uMatrix](https://github.com/gorhill/uMatrix) in restrictive blocking mode I always encounter an empty graph after unblocking and reloading the site.

This issue may occur with other adblockers as well and should be resolvable with the following workaround:
* hit `F12` (developer tools should pop up)
* check the `deactivate cache` checkbox
* reload the site
* (optional) hit `F12` again (to close the `developer tools`)
